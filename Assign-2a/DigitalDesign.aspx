﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="DigitalDesign.aspx.cs" Inherits="Assign_2a.DigitalDesign" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cssbackground" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <center><h1>Digital Design</h1></center>
    <h3>Joanna Komala</h3>
    <p>Timmings every Thursday from 8:00am to 11:35pm</p>
    <div class="row">
    <div class="col-md-6">
            <h2>Introduction to Digital design
            </h2>
            <p>
               Digital Design is an creative work practice that uses digital computer technology 
                as an essential part of the creative process. It is about the interplay between 
                people, computers and culture. Digital Design is an evolving and exciting 
                multi-disciplinary field that underlies many of today’s fast-growing industries  
                from information to communication, from advertising to entertainment. 
                It is everywhere..
            </p>
            </div>
        
    
        <div class="col-md-6">
            <h2>How to add form in HTML:</h2>
            <p>
                <h4>General syntax without angle brackets</h4>
               form</br>
                   First name:</br>
                    input type="text" name="firstname"</br>
                     Last name:</br>
                   input type="text" name="lastname"</br>
               form</br>
            </p>
            </div>
        </div>
    <div class="row">
    <div class="col-md-6">
            <h2>How to change background in css:</h2>
            <p>
              body {</br>
                    background-image: url("paper.gif");</br>
                    }</br>
            </p>
            
        </div>
        <div class="col-md-6">
            <h2>Some links to learn more:</h2>
            <p>
                 <a href='https://www.w3schools.com/css/css_background.asp'>Change Background</a></br>
               <a href='https://www.w3schools.com/html/html_forms.asp'>Add Forms</a 
            </p>
            </div>
        </div>
    
</asp:Content>
