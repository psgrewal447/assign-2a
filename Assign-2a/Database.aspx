﻿<%@ Page Title="Database" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="Assign_2a.Contact" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
   <center><h1>Database</h1></center> 
    <h3>Simon Borer</h3>
    <p>Timmings every Thursday from 11:40am to 3:15pm</p>

    <div class="row">
    <div class="col-md-6">
            <h2>Introduction of Database</h2>
            <p>A database is a collection of information that is organized so that it can be easily accessed, managed and updated.
                Data is organized into rows, columns and tables, and it is indexed to make it easier to find relevant 
                information. Data gets updated, expanded and deleted as new information is added. Databases process 
                workloads  to create and update themselves, querying the data they contain and running applications against it.
            </p>
            
        </div>
        <div class="col-md-6">
            <h2>How to create Tables:</h2>
            <p>
               <h4>General Syntax:</h4>
                CREATE TABLE table_name (
                   column1 datatype,
                         column2 datatype,
                             column3 datatype,
                                    ....
)                                      ;
            </p>
            </div>
        </div>
    <div class="row">
    <div class="col-md-6">
            <h2>How to enter data in tables:</h2>
            <p>
               
             INSERT INTO table_name (column1, column2, column3, ...)
               VALUES (value1, value2, value3, ...);

            </p>
            
        </div>
        <div class="col-md-6">
            <h2>Some references to the code above:</h2>
            <p>
               
                <a href='https://www.w3schools.com/sql/sql_create_table.asp'>Create Table Syntax</a></br>
                <a href='https://www.w3schools.com/sql/sql_insert.asp'>Insert into Tables</a>
                
            </p>
          </div>  
        </div>
    
</asp:Content>
