﻿<%@ Page Title="JavaScript" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="javascript.aspx.cs" Inherits="Assign_2a.About" %>

<asp:Content ID="BodyContent" ContentPlaceHolderID="MainContent" runat="server">
    <center><h1>Javascript</h1></center>
    <h3>SEAN Doyle</h3>
    <p>Timmings every monday from 10:45am to 2:15pm</p>
    <div class="row">
    <div class="col-md-6">
            <h2>Introduction to Javascript</h2>
            <p>
               JavaScript was initially created to “make webpages alive”.The programs in this language are called scripts. 
                They can be written right in the HTML and execute automatically as the page loads.Scripts are provided 
                and executed as a plain text. They don’t need a special preparation or a compilation to run..
            </p>
            
        </div>
        <div class="col-md-6">
            <h2>To ask user for input:</h2>
            <p>
              var person = prompt("Please enter your name", "Harry Potter");</br>
               if (person != null) {</br>
                    document.getElementById("demo").innerHTML =
                        "Hello " + person + "! How are you today?";</br>
}
            </p>
           </div> 
        </div>
    <div class="row">
    <div class="col-md-6">
            <h2>If Else statement in Javescript</h2>
            <p>
               if (condition) {</br>
               block of code to be executed if the condition is true </br>
               } </br>
                 else { </br>
                   block of code to be executed if the condition is false 
            </p>
            
        </div>
        <div class="col-md-6">
            <h2>Reference Links</h2>
            <p>
               <a href='https://www.w3schools.com/js/js_if_else.asp'>If/Else</a></br>
               <a href='https://www.w3schools.com/jsref/met_win_prompt.asp'>How user enter data in javascript</a 
            </p>
           </div> 
        </div>
    
</asp:Content>
